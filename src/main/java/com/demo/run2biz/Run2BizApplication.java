package com.demo.run2biz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Run2BizApplication {

    public static void main(String[] args) {
        SpringApplication.run(Run2BizApplication.class, args);
    }

}
