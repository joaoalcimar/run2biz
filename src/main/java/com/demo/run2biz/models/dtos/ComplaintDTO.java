package com.demo.run2biz.models.dtos;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * This object gathers all the necessary information about the understanding
 * of who made the complaint, and where it is located
 */
@Component
@Entity
@Setter
@Getter
@Table(name = "complaints")
public class ComplaintDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    //TODO: Improve serializing
    @Column
    private String complainer;

    @Column
    private String complaint;

    @Column
    private String address;

    @Override
    public String toString() {
        return "{\"data\": {" +
                "\"id\":\"" + id +
                "\", \"latitude\":" + latitude +
                ", \"longitude\":" + longitude +
                ", \"denunciante\": {\"" + complainer.toString() +
                "\"}, \"denuncia\": {\"" + complaint.toString() +
                "\"}, \"endereco\": {\"" + address +
                "}}}";
    }

}
