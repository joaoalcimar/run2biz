package com.demo.run2biz.utils;

import com.demo.run2biz.exceptions.BadRequestException;
import com.demo.run2biz.services.ComplaintService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;

import java.util.Map;

/**
 * <code>{@link ComplaintService}</code> responsible for auxiliary handling json data
 */
public class JsonHandlerUtil {

    private JsonHandlerUtil(){}

    /**
     * Get specific object in json array
     * @param jsonData The whole Data
     * @param fieldName The object wanted
     * @return Returns a String that represents the wanted object
     * @throws BadRequestException when conversion fails
     */
    public static String getObjectFromJson(Object jsonData, String fieldName) throws BadRequestException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonDataString = mapper.writeValueAsString(jsonData);

            Gson gson = new Gson();
            Map<Object, Double> jsonObj =
                    gson.fromJson(jsonDataString, Map.class);

            return gson.toJson(jsonObj.get(fieldName));

        } catch (JsonProcessingException e) {
            throw new BadRequestException("Error handling JSon from the request");
        }
    }

    /**
     *
     * @param jsonData The whole Data
     * @param fieldName The object wanted
     * @return Returns a Double from a json object
     * @throws BadRequestException when conversion fails
     */

    public static Double getDoubleFromJsonObject(Object jsonData, String fieldName) throws BadRequestException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonDataString = mapper.writeValueAsString(jsonData);

            Gson gson = new Gson();
            Map<Object, Double> jsonObj =
                    gson.fromJson(jsonDataString, Map.class);

            return jsonObj.get(fieldName);

        } catch (JsonProcessingException e) {
            throw new BadRequestException("Error handling JSon from the request");
        }
    }

    /**
     * Filters json data according to specific demand
     * @param data The json data
     * @param filter the metric used to filtering
     * @return
     */
    public static String filterJsonData(String data, String filter){
        return JsonPath.read(data, filter).toString();
    }
}
