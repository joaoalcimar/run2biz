package com.demo.run2biz.utils;

import com.demo.run2biz.exceptions.AutomationException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
@Component
public class RestTemplateUtil {

    private RestTemplateUtil(){}

    /**
     *
     * @param url of the request
     * @param method http verb
     * @param request request object containing headers
     * @return The Rest response of the request made
     * @throws AutomationException
     */
    public ResponseEntity<String> doRequest(String url, HttpMethod method, HttpEntity<String> request) throws AutomationException{
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, request, String.class);
    }
}
