package com.demo.run2biz.exceptions;

/**
 * <code>{@link BadRequestException}</code> abstraction to represent a 400 status code error
 */
public class BadRequestException extends Exception{

    public BadRequestException(String message) { super(message); }
}
