package com.demo.run2biz.controllers.v1;

import com.demo.run2biz.builders.ComplaintBuilder;
import com.demo.run2biz.exceptions.BadRequestException;
import com.demo.run2biz.exceptions.NotFoundException;
import com.demo.run2biz.models.dtos.ComplaintDTO;
import com.demo.run2biz.models.entities.Complainer;
import com.demo.run2biz.models.entities.Complaint;
import com.demo.run2biz.services.ComplaintService;
import com.demo.run2biz.utils.JsonHandlerUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The <code>ComplaintController<code/> Provides a controlling abstraction responsible for registering complaints
 */
@RestController
@RequestMapping("/v1")
public class ComplaintController {

    private static final Logger LOGGER = LogManager.getLogger(ComplaintController.class);

    @Autowired
    private ComplaintBuilder complaintBuilder;

    @Autowired
    private ComplaintService complaintService;

    /**
     *
     * @param dataInfo Object containing coordinates, data about the complainant and his complaint
     * @return A response entity with dataInfo response plus the address to its coordinates
     */
    @PostMapping(value = "/denuncias", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createComplaint (@RequestBody(required = false) Object dataInfo){

        try{
            ObjectMapper mapper = new ObjectMapper();
            Double latitude = JsonHandlerUtil.getDoubleFromJsonObject(dataInfo, "latitude");
            Double longitude = JsonHandlerUtil.getDoubleFromJsonObject(dataInfo, "longitude");
            Complainer complainer = mapper.readValue(JsonHandlerUtil.getObjectFromJson(dataInfo, "denunciante"), Complainer.class);
            Complaint complaint = mapper.readValue(JsonHandlerUtil.getObjectFromJson(dataInfo, "denuncia"), Complaint.class);

            ComplaintDTO complaintDTO = complaintBuilder.buildDTO(latitude, longitude, complainer, complaint);
            complaintService.saveComplaint(complaintDTO);
            LOGGER.info("Saving new Complaint with id= " + complaintDTO.getId() + " in Database");

            // TODO: Improve format
            return new ResponseEntity<>(complaintDTO.toString(), HttpStatus.CREATED);
        }catch (BadRequestException e){
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>("{\"message\": \"Request inválido.\"}", HttpStatus.BAD_REQUEST);
        }catch (NotFoundException e){
            LOGGER.error("{\"message\": \"Endereço não encontrado para essa localidade.\"}");
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }catch (Exception e){
            LOGGER.error("{\"message\": \"Erro interno do servidor.\"}");
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
