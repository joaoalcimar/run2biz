package com.demo.run2biz.clients;

import com.demo.run2biz.auxiliary.TestUtil;
import com.demo.run2biz.exceptions.AutomationException;
import com.demo.run2biz.exceptions.BadRequestException;
import com.demo.run2biz.exceptions.NotFoundException;
import com.demo.run2biz.utils.RestTemplateUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class GeoCodeClientTest {

    private RestTemplateUtil restTemplateUtil = mock(RestTemplateUtil.class);
    private GeoCodeClient geoCodeClient = new GeoCodeClient();

    private final String EXPECTED_URL = "http://www.mapquestapi.com/geocoding/v1/reverse?key=Some Key&location=1.0,2.0";

    @Before
    public void setUp(){
        geoCodeClient.setRestTemplateUtil(restTemplateUtil);
        geoCodeClient.setApiKey("Some Key");
    }

    @DisplayName("GeoCode Api Url build is correct")
    @Test
    public void buildApiUrlShouldWork(){
        assertEquals(EXPECTED_URL, geoCodeClient.buildAPIURL(1.0,2.0));
    }

    @DisplayName("GeoCode Api Location works fine")
    @Test
    public void getAPILocationShouldWork() throws AutomationException, NotFoundException, BadRequestException {
        ResponseEntity<String> response = new ResponseEntity<>(TestUtil.getValidGeoCodeApiResponse(), HttpStatus.OK);
        when(restTemplateUtil.doRequest(any(), any(), any())).thenReturn(response);
        assertDoesNotThrow(() -> geoCodeClient.getAPILocation(1.0,2.0));
    }

    @DisplayName("GeoCode Api request is bad")
    @Test
    public void getAPILocationShouldThrowBadRequest() throws AutomationException {
        ResponseEntity<String> response = new ResponseEntity<>(TestUtil.getBadRequestGeoCodeApiResponse(), HttpStatus.OK);
        when(restTemplateUtil.doRequest(any(), any(), any())).thenReturn(response);
        assertThrows(BadRequestException.class ,() -> geoCodeClient.getAPILocation(1.0,2.0));
    }

    @DisplayName("GeoCode Api Location cant find coordinates")
    @Test
    public void getAPILocationShouldThrowNotFound() throws AutomationException {
        ResponseEntity<String> response = new ResponseEntity<>(TestUtil.getNotFoundGeoCodeApiResponse(), HttpStatus.OK);
        when(restTemplateUtil.doRequest(any(), any(), any())).thenReturn(response);
        assertThrows(NotFoundException.class ,() -> geoCodeClient.getAPILocation(1.0,2.0));
    }

    @DisplayName("GeoCode Api has failed to contact")
    @Test
    public void getAPILocationShouldThrowAutomation() throws AutomationException {
        doThrow(AutomationException.class).when(restTemplateUtil).doRequest(any(), any(), any());
        assertThrows(NotFoundException.class ,() -> geoCodeClient.getAPILocation(1.0,2.0));
    }
}
