package com.demo.run2biz.builders;

import com.demo.run2biz.clients.GeoCodeClient;
import com.demo.run2biz.exceptions.BadRequestException;
import com.demo.run2biz.exceptions.NotFoundException;
import com.demo.run2biz.models.dtos.ComplaintDTO;
import com.demo.run2biz.models.entities.Address;
import com.demo.run2biz.models.entities.Complainer;
import com.demo.run2biz.models.entities.Complaint;
import com.demo.run2biz.utils.JsonHandlerUtil;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Setter
@Component
public class ComplaintBuilder {

    private static final Logger LOGGER = LogManager.getLogger(ComplaintBuilder.class);

    @Autowired
    private GeoCodeClient geoCodeClient;
    private final String STREET_API_FILTER = "$.results[0].locations[0].street";
    private final String NEIGHBORHOOD_API_FILTER = "$.results[0].locations[0].adminArea6";
    private final String CITY_API_FILTER = "$.results[0].locations[0].adminArea5";
    private final String STATE_API_FILTER = "$.results[0].locations[0].adminArea3";
    private final String COUNTRY_API_FILTER = "$.results[0].locations[0].adminArea1";
    private final String POSTAL_CODE_API_FILTER = "$.results[0].locations[0].postalCode";

    public ComplaintDTO buildDTO(Double latitude, Double longitude, Complainer complainer, Complaint denuncia)
            throws BadRequestException, NotFoundException {

        ComplaintDTO complaint = new ComplaintDTO();

        complaint.setLatitude(latitude);
        complaint.setLongitude(longitude);
        complaint.setComplainer(complainer.toString());
        complaint.setComplaint(denuncia.toString());
        complaint.setAddress(getAddress(latitude, longitude).toString());
        LOGGER.info("New Complaint Data Object Transfer was built.");
        return complaint;
    };

    protected Address getAddress(Double latitude, Double longitude) throws NotFoundException, BadRequestException {
        Address address = new Address();

        String apiData = geoCodeClient.getAPILocation(latitude, longitude);

        address.setBairro(JsonHandlerUtil.filterJsonData(apiData, NEIGHBORHOOD_API_FILTER));
        address.setLogradouro(JsonHandlerUtil.filterJsonData(apiData, STREET_API_FILTER));
        address.setCidade(JsonHandlerUtil.filterJsonData(apiData, CITY_API_FILTER));
        address.setEstado(JsonHandlerUtil.filterJsonData(apiData, STATE_API_FILTER));
        address.setPais(JsonHandlerUtil.filterJsonData(apiData, COUNTRY_API_FILTER));
        address.setCep(JsonHandlerUtil.filterJsonData(apiData, POSTAL_CODE_API_FILTER));

        return address;
    }
}
