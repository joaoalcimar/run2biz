package com.demo.run2biz.utils;

import com.demo.run2biz.auxiliary.TestUtil;
import com.demo.run2biz.exceptions.BadRequestException;

import com.google.gson.JsonSyntaxException;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(JUnit4.class)
public class JsonHandlerUtilTest {
    private final String INVALID_JSON_OBJECT = "{\"name\"\"John\", \"age\":30.0, \"car\":null";

    @DisplayName("Get Json object failed as expected")
    @Test
    public void getObjectFromJsonShouldThrowException(){
        assertThrows(JsonSyntaxException.class ,() -> JsonHandlerUtil.getObjectFromJson(INVALID_JSON_OBJECT, "age"));
    }

    @DisplayName("Get Double from Json failed as expected")
    @Test
    public void getDoubleFromJsonObjectShouldThrowException(){
        assertThrows(JsonSyntaxException.class ,() -> JsonHandlerUtil.getDoubleFromJsonObject(INVALID_JSON_OBJECT, "latitude"));
    }

    @DisplayName("Data filtering from Json is working properly")
    @Test
    public void filterJsonDataShouldWork(){
        assertEquals("404", JsonHandlerUtil.filterJsonData(TestUtil.getNotFoundGeoCodeApiResponse(), "$.info.statuscode"));
        assertEquals("400", JsonHandlerUtil.filterJsonData(TestUtil.getBadRequestGeoCodeApiResponse(), "$.info.statuscode"));
    }
}
