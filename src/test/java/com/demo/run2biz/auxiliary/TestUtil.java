package com.demo.run2biz.auxiliary;

public class TestUtil {

    public static String getInvalidGeoCodeApiResponse(){
        return "{\"info\":{\"statuscode\":0,\"copyright\":{\"text\":\"© 2022 MapQuest, Inc.\",\"imageUrl\":" +
                "\"http://api.mqcdn.com/res/mqlogo.gif\",\"imageAltText\":\"© 2022 MapQuest, Inc.\"},\"messages\":[]}," +
                "\"options\":{\"maxResults\":1,\"ignoreLatLngInput\":false},\"results\":[{\"providedLocation\"" +
                ":{\"latLng\":{\"lat\":-12,\"lng\":-24}},\"locations\":[{\"street\":\"\",\"adminArea6\":\"\"," +
                "\"adminArea6Type\":\"Neighborhood\",\"adminArea5\":\"\",\"adminArea5Type\":\"City\",\"adminArea4\":\"" +
                "\",\"adminArea4Type\":\"County\",\"adminArea3\":\"\",\"adminArea3Type\":\"State\",\"adminArea1\":\"\"," +
                "\"adminArea1Type\":\"Country\",\"postalCode\":\"\",\"geocodeQualityCode\":\"P1AAA\",\"geocodeQuality\"" +
                ":\"POINT\",\"dragPoint\":false,\"sideOfStreet\":\"N\",\"linkId\":\"0\",\"unknownInput\":\"\",\"type\"" +
                ":\"s\",\"latLng\":{\"lat\":-12,\"lng\":-24},\"displayLatLng\":{\"lat\":-12,\"lng\":-24},\"mapUrl\":\"\"}]}]}";
    }

    public static String getBadRequestGeoCodeApiResponse(){
        return "{\"info\":{\"statuscode\":400,\"copyright\":{\"text\":\"© 2022 MapQuest, Inc.\",\"imageUrl\":" +
                "\"http://api.mqcdn.com/res/mqlogo.gif\",\"imageAltText\":\"© 2022 MapQuest, Inc.\"},\"messages\":[]}," +
                "\"options\":{\"maxResults\":1,\"ignoreLatLngInput\":false},\"results\":[{\"providedLocation\"" +
                ":{\"latLng\":{\"lat\":-12,\"lng\":-24}},\"locations\":[{\"street\":\"\",\"adminArea6\":\"\"," +
                "\"adminArea6Type\":\"Neighborhood\",\"adminArea5\":\"\",\"adminArea5Type\":\"City\",\"adminArea4\":\"" +
                "\",\"adminArea4Type\":\"County\",\"adminArea3\":\"\",\"adminArea3Type\":\"State\",\"adminArea1\":\"\"," +
                "\"adminArea1Type\":\"Country\",\"postalCode\":\"\",\"geocodeQualityCode\":\"P1AAA\",\"geocodeQuality\"" +
                ":\"POINT\",\"dragPoint\":false,\"sideOfStreet\":\"N\",\"linkId\":\"0\",\"unknownInput\":\"\",\"type\"" +
                ":\"s\",\"latLng\":{\"lat\":-12,\"lng\":-24},\"displayLatLng\":{\"lat\":-12,\"lng\":-24},\"mapUrl\":\"\"}]}]}";
    }

    public static String getNotFoundGeoCodeApiResponse(){
        return "{\"info\":{\"statuscode\":404,\"copyright\":{\"text\":\"© 2022 MapQuest, Inc.\",\"imageUrl\":" +
                "\"http://api.mqcdn.com/res/mqlogo.gif\",\"imageAltText\":\"© 2022 MapQuest, Inc.\"},\"messages\":[]}," +
                "\"options\":{\"maxResults\":1,\"ignoreLatLngInput\":false},\"results\":[{\"providedLocation\"" +
                ":{\"latLng\":{\"lat\":-12,\"lng\":-24}},\"locations\":[{\"street\":\"\",\"adminArea6\":\"\"," +
                "\"adminArea6Type\":\"Neighborhood\",\"adminArea5\":\"\",\"adminArea5Type\":\"City\",\"adminArea4\":\"" +
                "\",\"adminArea4Type\":\"County\",\"adminArea3\":\"\",\"adminArea3Type\":\"State\",\"adminArea1\":\"\"," +
                "\"adminArea1Type\":\"Country\",\"postalCode\":\"\",\"geocodeQualityCode\":\"P1AAA\",\"geocodeQuality\"" +
                ":\"POINT\",\"dragPoint\":false,\"sideOfStreet\":\"N\",\"linkId\":\"0\",\"unknownInput\":\"\",\"type\"" +
                ":\"s\",\"latLng\":{\"lat\":-12,\"lng\":-24},\"displayLatLng\":{\"lat\":-12,\"lng\":-24},\"mapUrl\":\"\"}]}]}";
    }

    public static String getValidGeoCodeApiResponse(){
        return "{\"info\":{\"statuscode\":200,\"copyright\":{\"text\":\"© 2022 MapQuest, Inc.\",\"imageUrl\"" +
                ":\"http://api.mqcdn.com/res/mqlogo.gif\",\"imageAltText\":\"© 2022 MapQuest, Inc.\"},\"messages" +
                "\":[]},\"options\":{\"maxResults\":1,\"ignoreLatLngInput\":false},\"results\":[{\"providedLocation\"" +
                ":{\"latLng\":{\"lat\":-15.78,\"lng\":-47.88}},\"locations\":[{\"street\":\"ERL N\",\"adminArea6\"" +
                ":\"Asa Norte\",\"adminArea6Type\":\"Neighborhood\",\"adminArea5\":\"Brasília\",\"adminArea5Type\"" +
                ":\"City\",\"adminArea4\":\"\",\"adminArea4Type\":\"County\",\"adminArea3\":\"DF\",\"adminArea3Type\"" +
                ":\"State\",\"adminArea1\":\"BR\",\"adminArea1Type\":\"Country\",\"postalCode\":\"\"," +
                "\"geocodeQualityCode\":\"B1AAA\",\"geocodeQuality\":\"STREET\",\"dragPoint\":false,\"sideOfStreet\"" +
                ":\"R\",\"linkId\":\"0\",\"unknownInput\":\"\",\"type\":\"s\",\"latLng\":{\"lat\":-15.78," +
                "\"lng\":-47.88004},\"displayLatLng\":{\"lat\":-15.78,\"lng\":-47.88004},\"mapUrl\":\"\"" +
                ",\"roadMetadata\":{\"speedLimitUnits\":\"mph\",\"speedLimit\":37,\"tollRoad\":null}}]}]}";
    }
}
