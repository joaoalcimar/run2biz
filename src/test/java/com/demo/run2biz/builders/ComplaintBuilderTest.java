package com.demo.run2biz.builders;

import com.demo.run2biz.auxiliary.TestUtil;
import com.demo.run2biz.clients.GeoCodeClient;
import com.demo.run2biz.exceptions.BadRequestException;
import com.demo.run2biz.exceptions.NotFoundException;
import com.demo.run2biz.models.dtos.ComplaintDTO;
import com.demo.run2biz.models.entities.Address;
import com.demo.run2biz.models.entities.Complainer;
import com.demo.run2biz.models.entities.Complaint;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ComplaintBuilderTest {

    private GeoCodeClient geoCodeClient = mock(GeoCodeClient.class);

    private ComplaintBuilder complaintBuilder = new ComplaintBuilder();

    @Before
    public void setUp(){
        complaintBuilder.setGeoCodeClient(geoCodeClient);
    }

    @Test
    @DisplayName("Test creation of the Complaint DTO")
    public void buildDTOTestShouldWork() throws BadRequestException, NotFoundException {
        when(geoCodeClient.getAPILocation(any(), any())).thenReturn(TestUtil.getValidGeoCodeApiResponse());
        Double latitude = -14.0;
        Double longitude = -43.0;

        Complainer complainer = new Complainer();
        complainer.setCpf("111.111.111-11");
        complainer.setNome("Jane Doe");

        Complaint complaint = new Complaint();
        complaint.setDescricao("Some Description");
        complaint.setTitulo("Some Title");

        ComplaintDTO expectedDto = new ComplaintDTO();
        expectedDto.setLatitude(latitude);
        expectedDto.setLongitude(longitude);
        expectedDto.setComplainer(complainer.toString());
        expectedDto.setComplaint(complaint.toString());

        ComplaintDTO realDto = complaintBuilder.buildDTO(latitude, longitude, complainer, complaint);

        assertEquals(expectedDto.getLatitude(), realDto.getLatitude());
        assertEquals(expectedDto.getLongitude(), realDto.getLongitude());
        assertEquals(expectedDto.getComplainer(), realDto.getComplainer());
        assertEquals(expectedDto.getComplaint(), realDto.getComplaint());
    }

    @Test
    @DisplayName("Test creation of the Complaint DTO with blank entries")
    public void getAddressShouldNotFindLocation() throws NotFoundException, BadRequestException {
        when(geoCodeClient.getAPILocation(any(), any())).thenReturn(TestUtil.getInvalidGeoCodeApiResponse());

        Address realAddress = complaintBuilder.getAddress(1.0,1.0);

        assertEquals("", realAddress.getBairro());
        assertEquals("", realAddress.getPais());
        assertEquals("", realAddress.getCep());
        assertEquals("", realAddress.getEstado());
        assertEquals("", realAddress.getLogradouro());
        assertEquals("", realAddress.getCidade());
    }

    @Test
    @DisplayName("Test creation of the Complaint DTO with valid entries")
    public void getAddressShouldWork() throws NotFoundException, BadRequestException {
        when(geoCodeClient.getAPILocation(any(), any())).thenReturn(TestUtil.getValidGeoCodeApiResponse());

        Address realAddress = complaintBuilder.getAddress(1.0,1.0);

        assertEquals("Asa Norte", realAddress.getBairro());
        assertEquals("BR", realAddress.getPais());
        assertEquals("", realAddress.getCep());
        assertEquals("DF", realAddress.getEstado());
        assertEquals("ERL N", realAddress.getLogradouro());
        assertEquals("Brasília", realAddress.getCidade());
    }

}
