package com.demo.run2biz.constants;

/**
 * <code>{@link APIs}</code> related to API associated constants
 */
public class APIs {

    public final static String GEO_CODE_SRC_URL = "http://www.mapquestapi.com/geocoding/v1/reverse?";
}
