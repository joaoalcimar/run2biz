package com.demo.run2biz.services;

import com.demo.run2biz.models.dtos.ComplaintDTO;
import com.demo.run2biz.repositories.ComplaintDTORepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <code>{@link ComplaintService}</code> the Complaint DTO repo handler
 */
@Service
public class ComplaintService {

    @Autowired
    private ComplaintDTORepository complaintDTORepository;
    private static final Logger LOGGER = LogManager.getLogger(ComplaintService.class);

    /**
     * Save only one stance of the Complaint DTO
     * @param complaint the complaint to be saved in database
     */
    public void saveComplaint(ComplaintDTO complaint){
        complaintDTORepository.save(complaint);
        LOGGER.info("Complaint saved successfully.");
    }

}
