package com.demo.run2biz.models.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * Represents the complaint itself
 */
@Setter
@Getter
@Data
@Entity
public class Complaint {

    private String titulo;
    private String descricao;

    @Override
    public String toString() {
        return "titulo\":\"" + titulo
                + "\",\"descricao\":\"" + descricao;
    }
}
