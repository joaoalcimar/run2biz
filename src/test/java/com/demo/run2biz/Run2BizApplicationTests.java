package com.demo.run2biz;

import com.demo.run2biz.builders.ComplaintBuilderTest;
import com.demo.run2biz.clients.GeoCodeClientTest;
import com.demo.run2biz.utils.JsonHandlerUtilTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Suite.SuiteClasses({
        ComplaintBuilderTest.class,
        GeoCodeClientTest.class,
        JsonHandlerUtilTest.class,
})

    @RunWith(Suite.class)
    public class Run2BizApplicationTests {
    }
