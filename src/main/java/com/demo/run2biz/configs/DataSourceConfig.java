package com.demo.run2biz.configs;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * <code>{@link DataSourceConfig}</code> is responsible for creating the database connection
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "com.demo.run2biz.repositories",
        transactionManagerRef = "postgresTransactionManager",
        entityManagerFactoryRef = "postgresEntityManager")
public class DataSourceConfig {

    @Value("${vcap.services.custom-postgres.credentials.username:}")
    private String username;

    @Value("${vcap.services.custom-postgres.credentials.password:}")
    private String password;

    @Value("${vcap.services.custom-postgres.credentials.jdbc_uri:}")
    private String jdbcUri;

    @Bean(name = "postgresDataSource")
    @ConfigurationProperties(prefix = "spring.jpa")
    public DataSource postgresDataSource() {
        Properties props = new Properties();
        props.setProperty("poolName", "Run 2 Biz Pool");
        props.setProperty("driverClassName", "org.postgresql.Driver");
        props.setProperty("username", username);
        props.setProperty("password", password);
        props.setProperty("jdbcUrl", jdbcUri);
        props.setProperty("maximumPoolSize", "3");
        props.setProperty("minimumIdle", "2");
        HikariConfig config = new HikariConfig(props);
        HikariDataSource ds = new HikariDataSource(config);
        return ds;
    }

    @Bean(name = "postgresEntityManager")
    public LocalContainerEntityManagerFactoryBean postgresEntityManager(
            EntityManagerFactoryBuilder builder,
            @Qualifier("postgresDataSource") DataSource dataSource) {
        Map<String, Object> props = new HashMap<>();
        props.put(
                "hibernate.physical_naming_strategy",
                "org.hibernate.boot.model.naming.CamelCaseToUnderscoresNamingStrategy"
        );
        props.put(
                "hibernate.implicit_naming_strategy",
                "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy"
        );

        LocalContainerEntityManagerFactoryBean factoryBean = builder
                .dataSource(dataSource)
                .packages("com.demo.run2biz.models.*")
                .persistenceUnit("postgres")
                .build();
        factoryBean.setJpaPropertyMap(props);

        return factoryBean;
    }

    @Bean(name = "postgresTransactionManager")
    public PlatformTransactionManager postgresTransactionManager(
            @Qualifier("postgresEntityManager") EntityManagerFactory
                    postgresEntityManagerFactory
    ) {
        return new JpaTransactionManager(postgresEntityManagerFactory);
    }

}

