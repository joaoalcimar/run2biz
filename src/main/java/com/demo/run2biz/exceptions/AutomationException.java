package com.demo.run2biz.exceptions;

/**
 * <code>{@link AutomationException}</code> generic automation to generic purposes
 */
public class AutomationException extends Exception{

    public AutomationException(String message) { super(message); }
}
