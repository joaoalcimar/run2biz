package com.demo.run2biz.clients;

import com.demo.run2biz.constants.APIs;
import com.demo.run2biz.exceptions.AutomationException;
import com.demo.run2biz.exceptions.BadRequestException;
import com.demo.run2biz.exceptions.NotFoundException;
import com.demo.run2biz.utils.JsonHandlerUtil;
import com.demo.run2biz.utils.RestTemplateUtil;
import lombok.AccessLevel;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * <code>{@link GeoCodeClient}</code> connects to the GeoCode API, which maps the location according to the coordinates we pass to it
 */
@Component
@Setter(AccessLevel.PROTECTED)
public class GeoCodeClient {

    private static final Logger LOGGER = LogManager.getLogger(GeoCodeClient.class);

    private final String STATUS_CODE_FILTER = "$.info.statuscode";
    private final String BAD_REQUEST_CODE = "400";
    private final String NOT_FOUND_CODE = "404";

    @Value(value = "${vcap.services.geocode.credentials.apiKey:}")
    private String apiKey;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    /**
     *
     * @param latitude the latitude coordinate
     * @param longitude the longitude coordinate
     * @return The API Data containing information about the coordinates(e.g. city, country, state,...)
     * @throws NotFoundException is returned when any address is found in the send coordinates
     * @throws BadRequestException is returned when invalid coordinates are sent
     */
    public String getAPILocation(Double latitude, Double longitude) throws NotFoundException, BadRequestException {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> request = new HttpEntity<>(headers);
        LOGGER.info("Creating http request headers...");

        try{
            LOGGER.info("Calling GeoCode Api...");
            ResponseEntity<String> response = restTemplateUtil.doRequest(buildAPIURL(latitude, longitude), HttpMethod.GET, request);

            // GeoCode API returns 200 even if it's not
            String statusCode = JsonHandlerUtil.filterJsonData(response.getBody(), STATUS_CODE_FILTER);

            if (statusCode.equals(BAD_REQUEST_CODE)){
                throw new BadRequestException("Invalid Request.");
            }else if (statusCode.equals(NOT_FOUND_CODE)){
                throw new NotFoundException("Address not found.");
            }
            return response.getBody();
        } catch (AutomationException e) {
            LOGGER.error("Failed to call Geo Code API.");
            LOGGER.error(e.getMessage());
            throw new NotFoundException("Api Error Occurred.");
        }
    }

    /**
     *
     * @param latitude the latitude coordinate
     * @param longitude the longitude coordinate
     * @return The API Request needed to get more data about the address coordinates
     */
    protected String buildAPIURL(Double latitude, Double longitude){

        return APIs.GEO_CODE_SRC_URL.concat("key=").concat(apiKey).concat("&")
                .concat("location=").concat(String.valueOf(latitude)).concat(",").concat(String.valueOf(longitude));
    }
}
