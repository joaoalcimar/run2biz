package com.demo.run2biz.exceptions;

/**
 * <code>{@link NotFoundException}</code> abstraction to represent a 404 status code error
 */
public class NotFoundException extends Exception{

    public NotFoundException(String message) { super(message); }
}
