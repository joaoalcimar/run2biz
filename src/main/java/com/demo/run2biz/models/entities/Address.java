package com.demo.run2biz.models.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;

/**
 * Represents An Address DTO, containing information about the localization of complaint
 */
@Getter
@Setter
@Component
@Entity
public class Address {

    private String logradouro;
    private String bairro;
    private String cidade;
    private String estado;
    private String pais;
    private String cep;

    @Override
    public String toString() {
        return "logradouro\":\"" + logradouro + "\"" +
                ", \"bairro\":\"" + bairro + "\"" +
                ", \"cidade\":\"" + cidade + "\"" +
                ", \"estado\":\"" + estado + "\"" +
                ", \"pais\":\"" + pais + "\"" +
                ", \"cep\":\"" + cep + "\"";
    }
}
