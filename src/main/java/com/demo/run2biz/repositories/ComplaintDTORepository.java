package com.demo.run2biz.repositories;

import com.demo.run2biz.models.dtos.ComplaintDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <code>{@link ComplaintDTORepository}</code> CRUD to save Complaints DTO
 */
@Repository
public interface ComplaintDTORepository extends JpaRepository<ComplaintDTO, Long> {
}
