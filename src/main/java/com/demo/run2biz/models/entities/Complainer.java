package com.demo.run2biz.models.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

/**
 * Represents the entity who created the complaint
 */
@Getter
@Setter
@Data
@Entity
public class Complainer {
    String nome;
    String cpf;

    @Override
    public String toString() {
        return "nome\":\"" + nome +
                "\", \"cpf\":\"" + cpf;
    }
}
